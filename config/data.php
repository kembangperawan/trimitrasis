<?php

return [
    'main' => null,
    'address-challenge' => [
        array(
            'name' => 'Stay ahead of digital technology',
            'thumbnail' => '/public/images/we-address-challenge-1.png'
        ),
        array(
            'name' => 'Cross generation workforce challenges',
            'thumbnail' => '/public/images/we-address-challenge-2.png'
        ),
        array(
            'name' => 'Effectiveness logistic cost',
            'thumbnail' => '/public/images/we-address-challenge-3.png'
        ),
        array(
            'name' => 'Efficiencies operation cost',
            'thumbnail' => '/public/images/we-address-challenge-4.png'
        ),
        array(
            'name' => 'Focusing on customer engagement',
            'thumbnail' => '/public/images/we-address-challenge-5.png'
        )
    ],
    'solution-product' => [
        array(
            'name' => 'SAP',
            'description' => 'SAP is used by organization to integrate, organize, and maintain the data necessary for operational of business.',
            'thumbnail' => '/public/images/solution-product-sap.png'
        ),
        array(
            'name' => 'Salesforce',
            'description' => 'Trimitrasis will help you to integrate the functionality of marketing and sales, customer service, and business analysis',
            'thumbnail' => '/public/images/solution-product-salesforce.png'
        ),
        array(
            'name' => 'Manzel',
            'description' => 'Practice areas in the Information Technology solution and specializes in the development of applications.',
            'thumbnail' => '/public/images/solution-product-manzel.png'
        ),
        array(
            'name' => 'AMS',
            'description' => 'Committed to support your day to day business operation of the company.',
            'thumbnail' => '/public/images/solution-product-ams.png'
        )
    ],
    'solution-industry' => [
        array(
            'name' => 'Mining',
            'thumbnail' => '/public/images/solution-industry-mining.png'
        ),
        array(
            'name' => 'Heavy Equipment',
            'thumbnail' => '/public/images/solution-industry-heavy-equipment.png'
        ),
        array(
            'name' => 'Transportation & Logistic',
            'thumbnail' => '/public/images/solution-industry-transportation-logistic.png'
        ),
        array(
            'name' => 'Manufacturing',
            'thumbnail' => '/public/images/solution-industry-manufacturing.png'
        ),
        array(
            'name' => 'Property',
            'thumbnail' => '/public/images/solution-industry-property.png'
        )
    ],
    'core-value' => [
        array(
            'name' => 'Integrity',
            'thumbnail' => '/public/images/core-value-integrity.png'
        ),
        array(
            'name' => 'Professionalism',
            'thumbnail' => '/public/images/core-value-professionalism.png'
        ),
        array(
            'name' => 'Smart Working',
            'thumbnail' => '/public/images/core-value-smart-working.png'
        ),
        array(
            'name' => 'Business Insight',
            'thumbnail' => '/public/images/core-value-business-insight.png'
        ),
        array(
            'name' => 'Team Player',
            'thumbnail' => '/public/images/core-value-team-player.png'
        )
    ],
    'awesome-team' => [
        array(
            'name' => 'DWI HANDRI KURNIAWAN',
            'title' => 'BUSINESS DEVELOPMENT DIRECTOR',
            'thumbnail' => '/public/images/awesome-team-01.png'
        ),
        array(
            'name' => 'ALBERT JUANDA',
            'title' => 'PRESIDENT DIRECTOR',
            'thumbnail' => '/public/images/awesome-team-01.png'
        ),
        array(
            'name' => 'WIRYAWAN E. SUWADI',
            'title' => 'PROFESSIONAL SERVICE & SOLUTION ARCHITECT DIRECTOR',
            'thumbnail' => '/public/images/awesome-team-01.png'
        ),
        array(
            'name' => 'DERY HANANTO',
            'title' => 'BUSINESS DEVELOPMENT DIRECTOR',
            'thumbnail' => '/public/images/awesome-team-01.png'
        ),
        array(
            'name' => 'ADE PRIBADI',
            'title' => 'BUSINESS DEVELOPMENT DIRECTOR',
            'thumbnail' => '/public/images/awesome-team-01.png'
        ),
        array(
            'name' => 'DINA RETNO YULIANTI',
            'title' => 'PRESIDENT DIRECTOR',
            'thumbnail' => '/public/images/awesome-team-01.png'
        ),
        array(
            'name' => 'DIKA HARIS ABDURAHMAN',
            'title' => 'PROFESSIONAL SERVICE & SOLUTION ARCHITECT DIRECTOR',
            'thumbnail' => '/public/images/awesome-team-01.png'
        ),
        array(
            'name' => 'M. ARIF WAHYU SANTOSO',
            'title' => 'BUSINESS DEVELOPMENT DIRECTOR',
            'thumbnail' => '/public/images/awesome-team-01.png'
        ),
    ],
    'client-said' => [
        array(
            'company' => 'Komatsu',
            'name' => 'Mamad Zaelany',
            'logo' => '/public/images/client-say-brand-komatsu.png',
            'background' => '/public/images/client-say-company-background-1.png'
        ),
        array(
            'company' => 'Lotte',
            'name' => 'Sukaesih Purwanto',
            'logo' => '/public/images/client-say-brand-lotte.png',
            'background' => '/public/images/client-say-company-background-2.png'
        ),
        array(
            'company' => 'Komatsu',
            'name' => 'Mukhlis Yukihiro',
            'logo' => '/public/images/client-say-brand-komatsu.png',
            'background' => '/public/images/client-say-company-background-1.png'
        ),
        array(
            'company' => 'Lotte',
            'name' => 'Nandang Kilimanjaro',
            'logo' => '/public/images/client-say-brand-lotte.png',
            'background' => '/public/images/client-say-company-background-2.png'
        ),
    ]
];